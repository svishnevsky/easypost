﻿using Mapster;
using ShipmentApp.Data.Contracts.Entities;
using ShipmentApp.Domain.Contracts.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShipmentApp.Domain.Services.MappingProfiles
{
    public static class MapsterConfig
    {
        public static void ConfigCarrier()
        {
            TypeAdapterConfig<Carrier, CarrierViewModel>.NewConfig()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.Name, src => src.Name)
                .Map(dest => dest.NumberOfShipments, src => src.Shipments.Count);
        }
    }
}
