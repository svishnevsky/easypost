﻿using Mapster;
using ShipmentApp.Data.Contracts.Entities;
using ShipmentApp.Data.EntityFramework;
using ShipmentApp.Domain.Contracts;
using ShipmentApp.Domain.Contracts.ViewModels;
using ShipmentApp.Domain.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ShipmentApp.Domain.Services
{
    public class ShipmentService : IShipmentService
    {
        private readonly AppDbContext dbContext;
        public ShipmentService(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Create(ShipmentViewModel shipment)
        {
            shipment.EnsureExists();
            var result = TypeAdapter.Adapt<ShipmentViewModel, Shipment>(shipment);
            dbContext.Shipments.Add(result);
            dbContext.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var shipment = dbContext.Shipments.Find(id);
            shipment.EnsureExists();

            if (shipment.SenderAddress != null)
            {
                dbContext.Addresses.Remove(shipment.SenderAddress);
            }

            if (shipment.RecipientAddress != null)
            {
                dbContext.Addresses.Remove(shipment.RecipientAddress);
            }

            dbContext.Shipments.Remove(shipment);
            dbContext.SaveChanges();
        }

        public ShipmentViewModel Retrieve(Guid id)
        {
            var shipment = dbContext.Shipments.Find(id);
            shipment.EnsureExists();
            var result = TypeAdapter.Adapt<Shipment, ShipmentViewModel>(shipment);
            return result;
        }

        public IQueryable<ShipmentViewModel> RetrieveAll()
        {          
            var result = dbContext.Shipments.ProjectToType<ShipmentViewModel>();
            return result;
        }

        public void Update(ShipmentViewModel entity)
        {
            var shipment = dbContext.Shipments.Find(entity.Id);
            shipment.EnsureExists();
            var result = entity.Adapt(shipment);
            dbContext.Shipments.Update(result);
            dbContext.SaveChanges();
        }
    }
}
